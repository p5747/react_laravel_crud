<?php

use App\Http\Controllers\ProductController;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])
->name('dashboard');

Route::get('/api/users', function(){
    $users=User::all();
    return response()->json($users);
});

Route::post('/api/products', [ProductController::class, 'store']);

Route::get('/api/showproducts', [ProductController::class, 'index']);

Route::get('/api/show/{id}', [ProductController::class, 'show']);

require __DIR__.'/auth.php';

Route::get('/{any}', function(){
    return view ('welcome');
})->where('any', '.*');




