import axios from "axios";
import React, { useEffect, useState} from "react";
import ProductsList from "../components/ProductsList";

const Products = () => {

    const[state, setState]=useState([])

    useEffect(() => {
        axios.get("/api/showproducts").then((response) => {
            console.log(response);

            setState(response.data)
           
        });

    }, []);

    return (
        <>
            <h1>Products Page</h1>
            <ProductsList data={state}></ProductsList>
        </>
    );
};

export default Products;
