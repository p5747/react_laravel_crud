import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ProductDetail = () => {
    const params = useParams();
    const [product, setProduct] = useState([]);

    useEffect(() => {
        axios.get(`/api/show/${params.id}`).then((response) => {
            setProduct(response.data[0]);
        });
    }, []);

    console.log(product);

    return (
        <>
            <h1 className="my-5">Product Detail</h1>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-6 d-flex justify-content-center">
                        <div className="card" style={{ width: "18rem" }}>
                            <img
                                className="card-img-top"
                                src="https://picsum.photos/200"
                                alt="Card image cap"
                            ></img>
                            <div className="card-body">
                                <h5 className="card-title">{product.name}</h5>
                                <p className="card-text">
                                    Marca: {product.mark}
                                </p>
                                <p className="card-text">
                                    Oggetto numero: {product.id}. Articolo
                                    creato il: {product.created_at}
                                </p>
                                <a href="#" className="btn btn-primary">
                                    Go somewhere
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ProductDetail;
