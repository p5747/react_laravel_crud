import React from "react";
import ProductCard from "./ProductCard";

const ProductsList = (props) => {
    return(

        <>

        {props.data.map((el) => (
            <ProductCard key={el.id} id={el.id} name={el.mark} email={el.email} price={el.price} />
        ))} 

        </>

    

    );
}

export default ProductsList;