import React from "react";
import { Link } from "react-router-dom";

const ProductCard = (props) => {
    return (
        <div className="card my-2">
            <img src="..." className="card-img-top" alt="..."></img>
            <div className="card-body">
                <h5 className="card-title">{props.name}</h5>
                <p className="card-text">{props.mark}</p>
                <a href="#" className="btn btn-primary">
                    {props.price}
                </a>
                <Link to={`/products/${props.id}`}>Mostra dettaglio</Link>
            </div>
        </div>
    );
};

export default ProductCard;
