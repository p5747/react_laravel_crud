import React, { useState } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import ReactDOM from "react-dom";
import Navbar from "./UI/Navbar";
import UsersList from "./UsersList";
import { BrowserRouter } from "react-router-dom";
import Welcome from "../pages/Welcome";
import Products from "../pages/Products";
import ProductDetail from "../pages/ProductDetail";
import ProductInsert from "../pages/ProductInsert";

function App() {
    const [user, setUser] = useState([]);

    function getUsers() {
        /*  fetch('/api/users').then((response)=>{
           return response.json()
        }).then(data=>{
            console.log(data)
        })  */

        axios.get("/api/users").then((response) => {
            const trasformedData = response.data.map((el) => {
                return {
                    id: el.id,
                    name: el.name,
                    email: el.email,
                };
            });

            setUser(trasformedData);
        });
    }

    return (
        <>
            <header>
                <Navbar></Navbar>
            </header>

            <button className="btn btn-primary my-3" onClick={getUsers}>
                Clicca per il fetch
            </button>

            <UsersList users={user} />

            <Switch>
                <Route path="/" exact>
                    <Redirect to="/welcome" />
                </Route>
                <Route path="/welcome">
                    <Welcome />
                </Route>

                <Route path="/products" exact>
                    <Products />
                </Route>

                <Route path="/products/:id">
                    <ProductDetail />
                </Route>

                <Route path="/products-form">
                    <ProductInsert />
                </Route>
            </Switch>
        </>
    );
}

export default App;

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById("root")
);
