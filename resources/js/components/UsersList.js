import React from "react";
import Card from "./Card";

import styles from "./UsersList.module.css";

const UsersList = (props) => {
    function show() {
        console.log(props.users);
        console.log(props.users[0].id);
    }

    return (
        <>
            {props.users.map((el) => (
                <Card key={el.id} name={el.name} email={el.email} />
            ))}

            <button onClick={show}>Eccomi</button>
        </>
    );
};

export default UsersList;
