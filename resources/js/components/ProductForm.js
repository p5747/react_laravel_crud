import axios from "axios";
import React, {useState, useRef} from "react";
import { useHistory } from "react-router-dom";

const ProductForm = () => {

    const nameInputRef=useRef('')
    const markInputRef=useRef('')
    const priceInputRef=useRef(0)
    const history = useHistory()

    function submitHandler(event){

        event.preventDefault();

        const product = {

            name: nameInputRef.current.value,
            mark: markInputRef.current.value,
            price: priceInputRef.current.value
        }

        console.log(product)

        axios.post('/api/products', product).then((response)=>{
            console.log(response)
        })

        history.push('/products')

    }

    return (

        <div className="container-fluid">
            <div className="row justify-content-center">
                <div className="col-6">

                <form onSubmit={submitHandler}>
            <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                    Nome
                </label>
                <input
                    type="text"
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    ref={nameInputRef}
                    required
                ></input>
            </div>
            <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">
                    Marca
                </label>
                <input
                    type="text"
                    className="form-control"
                    id="exampleInputPassword1"
                    ref={markInputRef}
                    required
                ></input>
            </div>
            <div className="mb-3">
                <label htmlFor="exampleInputPassword2" className="form-label">
                    Prezzo
                </label>
                <input
                    type="number"
                    className="form-control"
                    id="exampleInputPassword2"
                    ref={priceInputRef}
                    required
                ></input>
            </div>
            <button type="submit" className="btn btn-primary">
                Invia
            </button>
        </form>

                </div>
            </div>
        </div>
        
    );
};

export default ProductForm;
